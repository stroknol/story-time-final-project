# User chooses the class monk
# Define exit function at the top
exit = lambda: sys.exit()
import l
# Main function

# Prompt user to choose a character
print("You awake and the bright blue sky is greeting you. You're laying on the ground, head pounding, and confused. The only thing you seem to remember is your name, and some set of skills.")
name  = input("What is your name? ")
print("Welcome, " + name + " to the land of Elden!")

print("Get ready to explore this wonderufl land, you will encounter many troubles, but you MUST be ready to face all challenges that come your way.")
print("Choose a character to hear their story:")
print("1. Mage")
print("2. Knight")
print("3. Monk")
choice = input("Enter your choice: ")


if choice.lower() == "mage":  
    print("Congradulations, you have selected the mage class!")
    print("This will be a very difficult journey, but you have the power of magic on your side.")
    print("When you selected this class, a magicaly staff appeared in your hand with a red magical crystal ball on the top of it")
    print("You begin to ponder how this happened. ")
    accept_weapon_mage = input("Will you accept this weapon? (yes/no): ")
    while True:
             if accept_weapon_mage.lower() == "yes":
                 print("You have accepted the weapon. The Dagger feels warm and powerful in your hands.")
                 print("With this sharp dagger, you feel ready to face any challenges that lie ahead.")
                 print("You begin your journey and notice a dark forest ahead.")
                 print("You proceed with caution as the vibes from the forest are off.")
                 print("You step inside the forest and it is as if the sun disappeared, you're surrounded by darkness.")
                 print("Your dagger drips blood and the darkness is dispelled, it's as if the staff reacted to your need.")
                 print("With the darkness gone you now notice a pack of ogres in the distance. It seems they are searching for something")
                 print("Ogre 1: I smell a mage!")
                 print("Ogre 2: What's that light over there!?")
                 print("You prepare yourself for a battle against two ogres")
                 live_or_die = input("You haven't much experience but you realized the staff reacts to your needs, do you run or attack (run or attack)? ")
                 if live_or_die.lower() == "attack":
                     print("You raise your dagger towards the incoming ogres, a touch of blood spews from the dagger.")
                     print("As your eyes adjust you notice two large objects on the ground in front of you.")
                     print("You blink and look back down realizing the two ogres are dead. You are pretty powerful.")
                     break
                 elif live_or_die.lower() == "run":
                     print("You turn tail and run as fast as you can. Your heart's beating louder than the sound of ogre feet pounding the ground behind you")
                     print("The bright blood from your dagger dissipates and your in the dark once more.")
                     print("You finally distance yourself from them and sit to rest.")
                     print("Ogre 1: Found you.")
                     print("You're dead!")
                     quit()
                 else:
                     print("try again")
                 # User declines the weapon
             elif accept_weapon_mage.lower() == "no":
                 print("You have chosen not to accept the weapon.")
                 print("Without the aid of your knife skills, your journey may be more difficult, but your determination will guide you.")
                 print("you make your way through the forest nearly blind, you hear two large creatures sniffing the air")
                 print("Ogre 1: I smell a mage!")
                 print("Ogre 2: I think I see them!")
                 print("With no weapon at hand, you don't survive the wild ogre attack")
                 print("You're dead!")
                 quit()        
#User advances through dark forest as mage
    while True:
         if live_or_die.lower() == "attack":
             print("As you examine the dead ogres you notice light peaking through the forest about 100 meters ahead.")
             print("You make your way towards the light and are greeted with an open field. The sun shining down upon you fills you with courage.")
             print("You notice a castle in the distance and make your way towards it.")
             print("You are greeted by the guards of the castle")
             print("Guard 1: Halt! State your name and purpose")
             print("I am " + name + " the " + choice)
             hostile_or_peacefull = input("You are given the chance to either attack or talk your way through peacefully.(violence or peace)")
             if hostile_or_peacefull == "peace":
                 print("I am but a mage of peace, I come to greet the lord of this castle and receive his blessing.")
                 print("The guards see to your truth and request a meeting between you and the lord.")
                 break
             elif hostile_or_peacefull == "violence":
                 print("You grab your staff and prepare to rush through the castle")
                 print("The guards at the front of the castle stand no chance against your magic.")
                 print("You make your way through the gate only to be met with 500 more guards. You underestimated the amount of troops that would be inside.")
                 print("You die!")
                 quit()
 #Chooses Peace
    while True:
         if hostile_or_peacefull == "peace":
             print("The guards advise you to follow them as they guide you to the throne room.")
             print("King: Hello mage, I have heard you traveled to receive my blessing. I will grant you this but only if you accept my request")
             print("There's a great evil that looms over these lands. You may have been attacked by ogres on your way here, he has sent his troops to kill anybody that might rival his power.")
             accept_or_deny_quest = input("I want you, powerful mage, to dispose of this evil. Accept this quest and receive my blessing, deny and face the consequences. (accept or deny) ")
             if accept_or_deny_quest == "accept":
                 print("King: Great! I will now increase your power tenfold, this will surely be enough to defeat the great evil that looms over these lands. As you leave this castle you will feel his presence, he has surely noticed yours and will be waiting at his lair. Follow this presence and get rid of this scourge.")
                 print("Before you leave the king sticks out his hands, you notice your body starts to glow and you feel great power within. Your staff glows with the light of 1000 suns, basking the entire throne room in holy light.")
                 print("The guards escort you out the castle, as soon as you step foot outside the castle you feel an immense pressure.")  
                 print("Great Evil: I sense a power that rivals my own, come and defeat me or face my wrath.")
                 break
             elif accept_or_deny_quest == "deny":
                 print("King: You were unfit to defeat the great evil anyways. guards dispose of this filth.")
                 print("You die!")
                 quit()
                 #Chooses accept
    while True:
         if accept_or_deny_quest == "accept":
             print("you follow the feeling of the immense pressure. It burdens your body with every step towards it.")
             print("As you approach the feeling you see the sky start to turn red.")
             print("You see a volcano in the distance, you know this is where your final battle will take place.")
             print("you approach the base of the volcano, your whole body is burning but you push your way through it. Your a mage after all, physical conditions don't affect the mind.")
             print("The great evil's forces rush towards you, but the king's blessing grants invulnerability towards such weak creatures.")
             print("You easily dispose of the minions and make your way towards the peak of the volcano.")
             print("You are face to face with the great evil. A large and grotesque demon awaits you.")
             print("Great Evil: So this is who the king has sent. I will say, you give off a powerful aura, but not powerful enough!")
             print("You notice the great evil begins to charge his power.")
             attack_or_meditate = input("You are given the chance to attack while the great evil powers up, or you can sit and meditate. (attack or meditate) ")
             if attack_or_meditate == "meditate":
                print("You close your eyes and sit. You feel power flowing through you like none before.")
                print("Great Evil: HAHAHAHAHA you think I will allow you to find peace before you die? Wrong!")
                print("You sit still, the great evil's words do not sway you. You focus on your teachings as a mage and continue to meditate.")
                print("The great evil charges towards you.")
                print("Great Evil: AHHHHHHHHH what's happening to me??")
                print("With your eyes still closed you sense the Great Evil's aura dissipating.")
                print("Great Evil: WHAT HAVE YOU DONE TO ME? HOW COULD I OF LOST TO A LOWLY MAGE?!")
                print("You open your eyes and the sky is blue once more. You sense no evil left in the world. You have completed your journey.")
                print("YOU WIN!!!!!")
                quit()
             elif attack_or_meditate == "attack":
                print("You grab your staff and prepare for the worst.")
                print("Great Evil: You are still so very weak, I can take you out with one foul swoop.")
                print("The great evil does just that. The great evil moves too fast for your eyes to see. Before you know it there's a giant hole where your stomach was.")
                print("Great Evil: lol too ez.")
                print("You die!")
                quit()
elif choice == "monk":
    print("Congratulations, you have selected the Monk class!")
    print("You begin to realize this will be a very difficult journey, but you have the strength of intellect on your side!")
    print("When you selected this class, a plain old staff appeared in front of you")
    print("You begin to ponder how this happened and where the staff came from")
    print("Suddenly, a magical figure comes out of nowhere and draws an old picture of the staff you are about to receive")
    accept_weapon_monk = input("Will you accept this weapon? (yes/no): ")
    while True:
            if accept_weapon_monk.lower() == "yes":
                print("You have accepted the weapon. The staff feels warm and powerful in your hands.")
                print("With this magical staff, you feel ready to face any challenges that lie ahead.")
                print("You begin your journey and notice a dark forest ahead.")
                print("You proceed with caution as the vibes from the forest are off.")
                print("You step inside the forest and it is as if the sun disappeared, you're surrounded by darkness.")
                print("Your staff lights up and the darkness is dispelled, it's as if the staff reacted to your need.")
                print("With the darkness gone you now notice a pack of ogres in the distance. It seems they are searching for something")
                print("Ogre 1: I smell a monk!")
                print("Ogre 2: What's that light over there!?")
                print("You prepare yourself for a battle against two ogres")
                live_or_die = input("You haven't much experience but you realized the staff reacts to your needs, do you run or attack (run or attack)? ")
                if live_or_die.lower() == "attack":
                    print("You raise your staff towards the incoming ogres, a blinding flash comes from the staff.")
                    print("As your eyes adjust you notice two large objects on the ground in front of you.")
                    print("You blink and look back down realizing the two ogres are dead. You are pretty powerful.")
                    break
                elif live_or_die.lower() == "run":
                    print("You turn tail and run as fast as you can. Your heart's beating louder than the sound of ogre feet pounding the ground behind you")
                    print("The light from your staff dissipates and your in the dark once more.")
                    print("You finally distance yourself from them and sit to rest.")
                    print("Ogre 1: Found you.")
                    print("You're dead!")
                    l.Turtle_L()
                    quit()
                else:
                    print("try again")
                # User declines the weapon
            elif accept_weapon_monk.lower() == "no":
                print("You have chosen not to accept the weapon.")
                print("Without the aid of magic, your journey may be more difficult, but your determination will guide you.")
                print("you make your way through the forest nearly blind, you hear two large creatures sniffing the air")
                print("Ogre 1: I smell a monk!")
                print("Ogre 2: I think I see them!")
                print("With no weapon at hand, you don't survive the wild ogre attack")
                print("You're dead!")
                quit()        
#User advances through dark forest as monk
    while True:
        if live_or_die.lower() == "attack":
            print("As you examine the dead ogres you notice light peaking through the forest about 100 meters ahead.")
            print("You make your way towards the light and are greeted with an open field. The sun shining down upon you fills you with courage.")
            print("You notice a castle in the distance and make your way towards it.")
            print("You are greeted by the guards of the castle")
            print("Guard 1: Halt! State your name and purpose")
            print("I am " + name + " the " + choice)
            hostile_or_peacefull = input("You are given the chance to either attack or talk your way through peacefully.(violence or peace)")
            if hostile_or_peacefull == "peace":
                print("I am but a monk of peace, I come to greet the lord of this castle and receive his blessing.")
                print("The guards see to your truth and request a meeting between you and the lord.")
                break
            elif hostile_or_peacefull == "violence":
                print("You grab your staff and prepare to rush through the castle")
                print("The guards at the front of the castle stand no chance against your magic.")
                print("You make your way through the gate only to be met with 500 more guards. You underestimated the amount of troops that would be inside.")
                print("You die!")
                quit()
#Chooses Peace
    while True:
        if hostile_or_peacefull == "peace":
            print("The guards advise you to follow them as they guide you to the throne room.")
            print("King: Hello monk, I have heard you traveled to receive my blessing. I will grant you this but only if you accept my request")
            print("There's a great evil that looms over these lands. You may have been attacked by ogres on your way here, he has sent his troops to kill anybody that might rival his power.")
            accept_or_deny_quest = input("I want you, powerful monk, to dispose of this evil. Accept this quest and receive my blessing, deny and face the consequences. (accept or deny) ")
            if accept_or_deny_quest == "accept":
                print("King: Great! I will now increase your power tenfold, this will surely be enough to defeat the great evil that looms over these lands. As you leave this castle you will feel his presence, he has surely noticed yours and will be waiting at his lair. Follow this presence and get rid of this scourge.")
                print("Before you leave the king sticks out his hands, you notice your body starts to glow and you feel great power within. Your staff glows with the light of 1000 suns, basking the entire throne room in holy light.")
                print("The guards escort you out the castle, as soon as you step foot outside the castle you feel an immense pressure.")  
                print("Great Evil: I sense a power that rivals my own, come and defeat me or face my wrath.")
                break
            elif accept_or_deny_quest == "deny":
                print("King: You were unfit to defeat the great evil anyways. guards dispose of this filth.")
                print("You die!")
                quit()
                #Chooses accept
    while True:
        if accept_or_deny_quest == "accept":
            print("you follow the feeling of the immense pressure. It burdens your body with every step towards it.")
            print("As you approach the feeling you see the sky start to turn red.")
            print("You see a volcano in the distance, you know this is where your final battle will take place.")
            print("you approach the base of the volcano, your whole body is burning but you push your way through it. Your a monk after all, physical conditions don't affect the mind.")
            print("The great evil's forces rush towards you, but the king's blessing grants invulnerability towards such weak creatures.")
            print("You easily dispose of the minions and make your way towards the peak of the volcano.")
            print("You are face to face with the great evil. A large and grotesque demon awaits you.")
            print("Great Evil: So this is who the king has sent. I will say, you give off a powerful aura, but not powerful enough!")
            print("You notice the great evil begins to charge his power.")
            attack_or_meditate = input("You are given the chance to attack while the great evil powers up, or you can sit and meditate. (attack or meditate) ")
            if attack_or_meditate == "meditate":
                print("You close your eyes and sit. You feel power flowing through you like none before.")
                print("Great Evil: HAHAHAHAHA you think I will allow you to find peace before you die? Wrong!")
                print("You sit still, the great evil's words do not sway you. You focus on your teachings as a monk and continue to meditate.")
                print("The great evil charges towards you.")
                print("Great Evil: AHHHHHHHHH what's happening to me??")
                print("With your eyes still closed you sense the Great Evil's aura dissipating.")
                print("Great Evil: WHAT HAVE YOU DONE TO ME? HOW COULD I OF LOST TO A LOWLY MONK?!")
                print("You open your eyes and the sky is blue once more. You sense no evil left in the world. You have completed your journey.")
                print("YOU WIN!!!!!")
                quit()
            elif attack_or_meditate == "attack":
                print("You grab your staff and prepare for the worst.")
                print("Great Evil: You are still so very weak, I can take you out with one foul swoop.")
                print("The great evil does just that. The great evil moves too fast for your eyes to see. Before you know it there's a giant hole where your stomach was.")
                print("Great Evil: lol too ez.")
                print("You die!")
                quit()

elif choice == "knight":  #change code \/
     print("Congratulations, you have selected the Knight class!")
     print("You begin to realize this will be a very difficult journey, but you have the strength of intellect on your side!")
     print("When you selected this class, a plain old broadsword appeared in front of you")
     print("You begin to ponder how this happened and where the broadsword came from")
     print("Suddenly, a magical figure comes out of nowhere and draws an old picture of the broadsword you are about to receive")
     accept_weapon_knight = input("Will you accept this weapon? (yes/no): ")
     while True:
         if accept_weapon_knight.lower() == "yes":
             print("You have accepted the weapon. The broadsword feels warm and powerful in your hands.")
             print("With this magical broadsword, you feel ready to face any challenges that lie ahead.")
             print("You begin your journey and notice a dark forest ahead.")
             print("You proceed with caution as the vibes from the forest are off.")
             print("You step inside the forest and it is as if the sun disappeared, you're surrounded by darkness.")
             print("Your broadsword lights up and the darkness is dispelled, it's as if the broadsword reacted to your need.")
             print("With the darkness gone you now notice a pack of ogres in the distance. It seems they are searching for something")
             print("Ogre 1: I smell a monk!")
             print("Ogre 2: What's that light over there!?")
             print("You prepare yourself for a battle against two ogres")
             live_or_die = input("You haven't much experience but you realized the broadsword reacts to your needs, do you run or attack (run or attack)? ")
             if live_or_die.lower() == "attack":
                 print("You raise your broadsword towards the incoming ogres, a blinding flash comes from the broadsword.")
                 print("As your eyes adjust you notice two large objects on the ground in front of you.")
                 print("You blink and look back down realizing the two ogres are dead. You are pretty powerful.")
                 break
             elif live_or_die.lower() == "run":
                 print("You turn tail and run as fast as you can. Your heart's beating louder than the sound of ogre feet pounding the ground behind you")
                 print("The light from your broadsword dissipates and your in the dark once more.")
                 print("You finally distance yourself from them and sit to rest.")
                 print("Ogre 1: Found you.")
                 print("You're dead!")
                 quit()
             else:
                 print("try again")
             # User declines the weapon
         elif accept_weapon_knight.lower() == "no":
             print("You have chosen not to accept the weapon.")
             print("Without the aid of magic, your journey may be more difficult, but your determination will guide you.")
             print("you make your way through the forest nearly blind, you hear two large creatures sniffing the air")
             print("Ogre 1: I smell a knight!")
             print("Ogre 2: I think I see them!")
             print("With no weapon at hand, you don't survive the wild ogre attack")
             print("You're dead!")
             quit()        
     #User advances through dark forest as knight
     while True:
         if live_or_die.lower() == "attack":
             print("As you examine the dead ogres you notice light peaking through the forest about 100 meters ahead.")
             print("You make your way towards the light and are greeted with an open field. The sun shining down upon you fills you with courage.")
             print("You notice a castle in the distance and make your way towards it.")
             print("You are greeted by the guards of the castle")
             print("Guard 1: Halt! State your name and purpose")
             print("I am " + name + " the " + choice)
             hostile_or_peacefull = input("You are given the chance to either attack or talk your way through peacefully.(violence or peace)")
             if hostile_or_peacefull == "peace":
                 print("I am but a knight of peace, I come to greet the lord of this castle and receive his blessing.")
                 print("The guards see to your truth and request a meeting between you and the lord.")
                 break
             elif hostile_or_peacefull == "violence":
                 print("You grab your broadsword and prepare to rush through the castle")
                 print("The guards at the front of the castle stand no chance against your magic.")
                 print("You make your way through the gate only to be met with 500 more guards. You underestimated the amount of troops that would be inside.")
             print("You die!")
             l.Turtle_L()

             quit()
 #Chooses Peace
     while True:
         if hostile_or_peacefull == "peace":
             print("The guards advise you to follow them as they guide you to the throne room.")
             print("King: Hello knight, I have heard you traveled to receive my blessing. I will grant you this but only if you accept my request")
             print("There's a great evil that looms over these lands. You may have been attacked by ogres on your way here, he has sent his troops to kill anybody that might rival his power.")
             accept_or_deny_quest = input("I want you, powerful knight, to dispose of this evil. Accept this quest and receive my blessing, deny and face the consequences. (accept or deny) ")
             if accept_or_deny_quest == "accept":
                 print("King: Great! I will now increase your power tenfold, this will surely be enough to defeat the great evil that looms over these lands. As you leave this castle you will feel his presence, he has surely noticed yours and will be waiting at his lair. Follow this presence and get rid of this scourge.")
                 print("Before you leave the king sticks out his hands, you notice your body starts to glow and you feel great power within. Your staff glows with the light of 1000 suns, basking the entire throne room in holy light.")
                 print("The guards escort you out the castle, as soon as you step foot outside the castle you feel an immense pressure.")  
                 print("Great Evil: I sense a power that rivals my own, come and defeat me or face my wrath.")
                 break
             elif accept_or_deny_quest == "deny":
                 print("King: You were unfit to defeat the great evil anyways. guards dispose of this filth.")
                 print("You die!")
                 quit()
                 #Chooses accept
     while True:
         if accept_or_deny_quest == "accept":
             print("you follow the feeling of the immense pressure. It burdens your body with every step towards it.")
             print("As you approach the feeling you see the sky start to turn red.")
             print("You see a volcano in the distance, you know this is where your final battle will take place.")
             print("you approach the base of the volcano, your whole body is burning but you push your way through it. Your a knight after all, physical conditions don't affect the mind.")
             print("The great evil's forces rush towards you, but the king's blessing grants invulnerability towards such weak creatures.")
             print("You easily dispose of the minions and make your way towards the peak of the volcano.")
             print("You are face to face with the great evil. A large and grotesque demon awaits you.")
             print("Great Evil: So this is who the king has sent. I will say, you give off a powerful aura, but not powerful enough!")
             print("You notice the great evil begins to charge his power.")
             attack_or_meditate = input("You are given the chance to attack while the great evil powers up, or you can sit and meditate. (attack or meditate) ")
             if attack_or_meditate == "meditate":
                 print("You close your eyes and sit. You feel power flowing through you like none before.")
                 print("Great Evil: HAHAHAHAHA you think I will allow you to find peace before you die? Wrong!")
                 print("You sit still, the great evil's words do not sway you. You focus on your teachings as a knight and continue to meditate.")
                 print("The great evil charges towards you.")
                 print("Great Evil: AHHHHHHHHH what's happening to me??")
                 print("With your eyes still closed you sense the Great Evil's aura dissipating.")
                 print("Great Evil: WHAT HAVE YOU DONE TO ME? HOW COULD I OF LOST TO A LOWLY KNIGHT?!")
                 print("You open your eyes and the sky is blue once more. You sense no evil left in the world. You have completed your journey.")
                 print("YOU WIN!!!!!")
                 quit()
             elif attack_or_meditate == "attack":
                 print("You grab your broadsword and prepare for the worst.")
                 print("Great Evil: You are still so very weak, I can take you out with one foul swoop.")
                 print("The great evil does just that. The great evil moves too fast for your eyes to see. Before you know it there's a giant hole where your stomach was.")
                 print("Great Evil: lol too ez.")
                 print("You die!")
                 l.Turtle_L()
                 quit()

